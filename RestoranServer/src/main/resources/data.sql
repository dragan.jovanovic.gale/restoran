INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');


INSERT INTO kategorija (id,naziv) VALUES (1,'Pizza');
INSERT INTO kategorija (id,naziv) VALUES (2,'Dessert');
INSERT INTO kategorija (id,naziv) VALUES (3,'Vine');


INSERT INTO jelovnik (id,naziv, cena,kategorija_id) VALUES (1,'Pizza Romana', 1200 ,1);
INSERT INTO jelovnik (id,naziv, cena,kategorija_id) VALUES (2,'Pizza Viennes', 1400, 1);
INSERT INTO jelovnik (id,naziv, cena,kategorija_id) VALUES (3,'Pizza Cappricose', 1500, 1);
INSERT INTO jelovnik (id,naziv, cena,kategorija_id) VALUES (4,'Cianti', 850, 3);
INSERT INTO jelovnik (id,naziv, cena,kategorija_id) VALUES (5,'Tiramisu', 750, 2);





