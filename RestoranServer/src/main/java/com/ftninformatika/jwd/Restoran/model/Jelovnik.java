package com.ftninformatika.jwd.Restoran.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Jelovnik {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false,unique = true)
	private String naziv;
	
	@Column(nullable = false,unique = false)
	private Double cena;
	
	@ManyToOne
	private Kategorija kategorija;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}

	

	public Kategorija getKategorija() {
		return kategorija;
	}


	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}


	public Jelovnik() {
		super();
	}


	public Jelovnik(Long id, String naziv, Double cena, Kategorija kategorija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.kategorija = kategorija;
	}




	
	
	
	
}
