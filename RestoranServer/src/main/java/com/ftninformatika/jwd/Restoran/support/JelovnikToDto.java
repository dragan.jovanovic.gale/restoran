package com.ftninformatika.jwd.Restoran.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.Restoran.dto.JelovnikDto;
import com.ftninformatika.jwd.Restoran.model.Jelovnik;
import com.ftninformatika.jwd.Restoran.service.KategorijaService;
@Component
public class JelovnikToDto implements Converter<Jelovnik, JelovnikDto> {

	@Autowired
	private KategorijaService service;
	
	@Override
	public JelovnikDto convert(Jelovnik source) {
		JelovnikDto dto=new JelovnikDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setCena(source.getCena());
		dto.setKategorija(source.getKategorija());
		
		return dto;
	}
	
	public List <JelovnikDto> convert(List <Jelovnik> list) {
		List <JelovnikDto> dto=new ArrayList<>();
		for(Jelovnik jelovnik:list) {
			dto.add(convert(jelovnik));
		}
		return dto;
		
	}

}
