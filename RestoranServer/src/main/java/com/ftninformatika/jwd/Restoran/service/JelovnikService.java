package com.ftninformatika.jwd.Restoran.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.Restoran.model.Jelovnik;

public interface JelovnikService {
	
	Jelovnik findOneById(Long id);
	
	List<Jelovnik> findAll();
	
	Page <Jelovnik> find(String naziv,Integer pageNo);

	Jelovnik save(Jelovnik jelovnik);

	Jelovnik update(Jelovnik t);
	
	Jelovnik delete(Long id);

}
