package com.ftninformatika.jwd.Restoran.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.Restoran.dto.KategorijaDto;
import com.ftninformatika.jwd.Restoran.model.Kategorija;

@Component
public class KategorijaToDto implements Converter<Kategorija, KategorijaDto>{

	@Override
	public KategorijaDto convert(Kategorija source) {
		KategorijaDto dto=new KategorijaDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		return dto;
	}
	
	public List <KategorijaDto> convert(List <Kategorija> list) {
		List <KategorijaDto> dto=new ArrayList<>();
		for(Kategorija kategorija:list) {
			dto.add(convert(kategorija));
		}
		return dto;
		
	}
}
