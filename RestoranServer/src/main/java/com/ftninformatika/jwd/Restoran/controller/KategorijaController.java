package com.ftninformatika.jwd.Restoran.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.Restoran.dto.KategorijaDto;
import com.ftninformatika.jwd.Restoran.model.Kategorija;
import com.ftninformatika.jwd.Restoran.service.KategorijaService;
import com.ftninformatika.jwd.Restoran.support.DtoToKategorija;
import com.ftninformatika.jwd.Restoran.support.KategorijaToDto;

@RestController
@RequestMapping(value = "/api/kategorije",produces = MediaType.APPLICATION_JSON_VALUE)
public class KategorijaController {
	
	@Autowired
	private KategorijaService service;
	
	@Autowired
	private KategorijaToDto toDto;
	
	@Autowired
	private DtoToKategorija toEntity;
	
//	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<KategorijaDto>> getAll() {
		List<Kategorija> list=service.findAll();
		return new ResponseEntity<>(toDto.convert(list),HttpStatus.OK);
	}
		
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<KategorijaDto> getOne(@PathVariable Long id) {
		Kategorija kategorija = service.findOneById(id); 

		if (kategorija != null) {
			return new ResponseEntity<>(toDto.convert(kategorija), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
//	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<KategorijaDto> create(@Valid @RequestBody KategorijaDto kategorijaDto) { 
		Kategorija kategorija = toEntity.convert(kategorijaDto);

		Kategorija sacuvanaKategorija = service.save(kategorija);

		return new ResponseEntity<>(toDto.convert(sacuvanaKategorija), HttpStatus.CREATED);
	}
	
//	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<KategorijaDto> update(@PathVariable Long id, @Valid @RequestBody KategorijaDto kategorijaDto) { 

		if (!id.equals(kategorijaDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (service.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Kategorija kategorija = toEntity.convert(kategorijaDto); 

		Kategorija sacuvanaKategorija = service.update(kategorija);

		return new ResponseEntity<>(toDto.convert(sacuvanaKategorija), HttpStatus.OK);
	}
	
//	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Kategorija obrisanaKategorija = service.findOneById(id);

		if (obrisanaKategorija != null) {
			service.delete(obrisanaKategorija.getId());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}