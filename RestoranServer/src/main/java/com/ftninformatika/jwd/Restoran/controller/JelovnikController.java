package com.ftninformatika.jwd.Restoran.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.Restoran.dto.JelovnikDto;
import com.ftninformatika.jwd.Restoran.model.Jelovnik;
import com.ftninformatika.jwd.Restoran.service.JelovnikService;
import com.ftninformatika.jwd.Restoran.support.DtoToJelovnik;
import com.ftninformatika.jwd.Restoran.support.JelovnikToDto;

@RestController
@RequestMapping(value = "/api/jelovnik",produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:8080")
public class JelovnikController {
	
	@Autowired
	private JelovnikService service;
	
	@Autowired
	private JelovnikToDto toDto;
	
	@Autowired
	private DtoToJelovnik toEntity;
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<JelovnikDto>> getAll(
			@RequestParam(required=false) String naziv, 
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		

		Page<Jelovnik> page = service.find(naziv, pageNo) ;

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<JelovnikDto> getOne(@PathVariable Long id) {
		Jelovnik prijava = service.findOneById(id); 

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
//	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JelovnikDto> create(@Valid @RequestBody JelovnikDto jelovnikDto) { 
		Jelovnik jelovnik = toEntity.convert(jelovnikDto);

		Jelovnik sacuvaniTakmicar = service.save(jelovnik);

		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
	}
	
//	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JelovnikDto> update(@PathVariable Long id, @Valid @RequestBody JelovnikDto jelovnikDto) { 

		System.out.println(jelovnikDto);
		if (!id.equals(jelovnikDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (service.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Jelovnik jelovnik = toEntity.convert(jelovnikDto); 

		Jelovnik sacuvanJelovnik = service.update(jelovnik);

		return new ResponseEntity<>(toDto.convert(sacuvanJelovnik), HttpStatus.OK);
	}
	

//	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Jelovnik obrisanilovnik = service.findOneById(id);
		if (obrisanilovnik != null) {
			service.delete(obrisanilovnik.getId());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	

}
