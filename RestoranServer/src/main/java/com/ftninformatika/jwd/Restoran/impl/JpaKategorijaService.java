package com.ftninformatika.jwd.Restoran.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.Restoran.model.Jelovnik;
import com.ftninformatika.jwd.Restoran.model.Kategorija;
import com.ftninformatika.jwd.Restoran.repository.KategorijaRepository;
import com.ftninformatika.jwd.Restoran.service.KategorijaService;
@Service
public class JpaKategorijaService implements KategorijaService{

	
	@Autowired
	private KategorijaRepository rep;
	
	@Override
	public Kategorija findOneById(Long id) {
		return rep.findOnById(id);
	}

	@Override
	public List<Kategorija> findAll() {
		return rep.findAll();
	}

	@Override
	public Kategorija save(Kategorija kategorija) {
		return rep.save(kategorija);
	}

	@Override
	public Kategorija update(Kategorija kategorija) {
		// TODO Auto-generated method stub
		return rep.save(kategorija);
	}

	@Override
	public Kategorija delete(Long id) {
		Optional<Kategorija> kategorija=Optional.of(findOneById(id));
		if(kategorija.isPresent()) {
			rep.deleteById(id);
			return kategorija.get();
		}
		return null;
	}

}
