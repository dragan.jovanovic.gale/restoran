package com.ftninformatika.jwd.Restoran.dto;

import com.ftninformatika.jwd.Restoran.model.Kategorija;

public class JelovnikDto {
	
	private Long id;
	
	private String naziv;
	
	private Double cena;
	
	private Kategorija kategorija;
	


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	@Override
	public String toString() {
		return "JelovnikDto [id=" + id + ", naziv=" + naziv + ", cena=" + cena + ", kategorija=" + kategorija + "]";
	}





	
	
	
	

}
