package com.ftninformatika.jwd.Restoran.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Kategorija {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false,unique = true)
	private String naziv;
	
	@OneToMany(mappedBy = "kategorija",cascade = CascadeType.ALL)
	private List<Jelovnik> jelovnici=new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	
	
	public Kategorija() {
		super();
	}

	public Kategorija(Long id, String naziv, List<Jelovnik> jelovnici) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.jelovnici = jelovnici;
	}


	
	
	

}
