package com.ftninformatika.jwd.Restoran.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.Restoran.model.Jelovnik;

@Repository
public interface JelovnikRepository extends JpaRepository<Jelovnik, Long>{
	
	Jelovnik findOneById(Long id);
	
	Page<Jelovnik> findByNazivIgnoreCaseContains(String naziv, Pageable p);

}
