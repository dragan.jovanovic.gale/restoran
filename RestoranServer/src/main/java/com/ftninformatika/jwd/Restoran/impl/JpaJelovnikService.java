package com.ftninformatika.jwd.Restoran.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.Restoran.model.Jelovnik;
import com.ftninformatika.jwd.Restoran.repository.JelovnikRepository;
import com.ftninformatika.jwd.Restoran.service.JelovnikService;

@Service
public class JpaJelovnikService implements JelovnikService{
	
	@Autowired
	private JelovnikRepository rep;

	@Override
	public Jelovnik findOneById(Long id) {
		return rep.findOneById(id);
	}

	@Override
	public List<Jelovnik> findAll() {
		// TODO Auto-generated method stub
		return rep.findAll();
	}
	
	@Override
	public Jelovnik save(Jelovnik jelovnik) {
		// TODO Auto-generated method stub
		return rep.save(jelovnik);
	}

	@Override
	public Jelovnik update(Jelovnik t) {
		// TODO Auto-generated method stub
		return rep.save(t);
	}

	public Jelovnik delete(Long id) {
		Optional<Jelovnik> jelovnik=Optional.of(findOneById(id));
		if(jelovnik.isPresent()) {
			rep.deleteById(id);
			return jelovnik.get();
		}
		return null;
	}

	@Override
	public Page<Jelovnik> find(String naziv, Integer pageNo) {
		if(naziv==null) {
			naziv="";
			return rep.findByNazivIgnoreCaseContains(naziv, PageRequest.of(pageNo, 2));
		}
		return rep.findByNazivIgnoreCaseContains(naziv, PageRequest.of(pageNo, 2));
	}

}
