package com.ftninformatika.jwd.Restoran.service;

import java.util.List;

import com.ftninformatika.jwd.Restoran.model.Kategorija;

public interface KategorijaService {

	Kategorija findOneById(Long id);
	
	List<Kategorija> findAll();

	Kategorija save(Kategorija kategorija);

	Kategorija update(Kategorija kategorija);

	Kategorija delete(Long id);
}
