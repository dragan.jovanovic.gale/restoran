import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Food } from '../model/Food.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { FoodDto } from '../DTO/Food.dto';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],

})


export class MainComponent implements OnInit {
  public previewNewCommentSection: boolean[] = [];
  public foods: Food[];
  public logged: boolean;
  public pageNo:  number;
  public food: Food;
  public foodDto: FoodDto;
  public isChecked = false;
  public valueID: number;
  public loggedm: boolean;
  public pretraga: string;
  

  

  constructor(private appService: AppService ,private http: HttpClient,private route: Router) {
    this.pretraga="";
    this.loggedm=false;
    this.valueID=-1;
    this.foodDto={
      id:-1,
      cena:0,
      naziv:'',
    }
    this.foods = [];
    this.pageNo=0; 
    this.food={
      id:-1,
      naziv:'',
      kategorija:{
        id:-1,
        naziv:'',
      },
      cena:0,
    }

    for (let food of this.foods) {
      this.previewNewCommentSection.push(false);
    }
    localStorage.getItem('user') === null
      ? (this.logged = false)
      : (this.logged = true);
      
  }

  ngOnInit(): void {
  this.appService.getMenu(this.pageNo,this.pretraga).subscribe((res) => (this.foods = res));
    
    this.appService.editUser(localStorage.getItem('user'));
  }

  nextPage() {
    this.pageNo=this.pageNo+1;
    this.appService.getMenu(this.pageNo,this.pretraga).subscribe((res) => (this.foods = res));
    console.log(this.pageNo);
    
      }
    
    previousPage() {
      this.pageNo=this.pageNo-1;
      this.appService.getMenu(this.pageNo,this.pretraga).subscribe((res) => (this.foods = res));{
      };
        }


     deleteFood(id:any) {
      console.log(id);
        this.appService.deleteFood(id).subscribe((res) => (this.foods = res));{
          window.location.reload();
          };
            }  

            
    resetFood(food: Food) {
      this.appService.resetFood(food);
         }

    showFormToggle(id:any){
          this.valueID=id;
          console.log("this is in showfORM"+this.valueID)
          this.isChecked = !this.isChecked;
      }

      updateFood(food:Food,id: any){
        this.foodDto.naziv=food.naziv;
        this.foodDto.cena=food.cena;
        this.foodDto.id=this.valueID;
        this.appService.updateFood(this.foodDto).subscribe(res=>console.log(res));
        window.location.reload();
      }

      search(pretraga:string){
        this.appService.getMenu(this.pageNo,this.pretraga).subscribe((res) => (this.foods = res));
        this.pageNo=0;
      }


   }

