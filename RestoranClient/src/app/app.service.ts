import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { User } from './model/User.model';
import { UserDto } from './DTO/User.dto';
import { Food } from './model/Food.model';
import { FoodDto } from './DTO/Food.dto';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private currentUser = new BehaviorSubject<User | null>(null);

  cast = this.currentUser.asObservable();

  public loggedUser: User | null;
  constructor(private http: HttpClient) {
    this.loggedUser = null;
  }

 

  register(user: User) {
    return this.http.post<any>(`http://localhost:8080/api/korisnici`, user);
  }

  login(userDto: UserDto) {
    return this.http.post<any>(`http://localhost:8080/api/korisnici/auth`, userDto);
  }

  editUser(newUser: any) {
    this.currentUser.next(newUser);
  }

  getMenu(pageNo:number,pretraga:string) {
    console.log(pretraga)
    console.log(pageNo)
    return this.http.get<Food[]>(`http://localhost:8080/api/jelovnik/?naziv=${pretraga}&pageNo=${pageNo}`);
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  // getLastPage(pageNo:number){
  //   return this.http.get<any>(`http://localhost:8080/api/jelovnik/?pageNo=${pageNo}`, {observe: 'response'}).subscribe(resp => { console.log(resp.headers.get('Total-Pages'));
  // });
  // }

  deleteFood(id: number|undefined){
    return this.http.delete<any>(`http://localhost:8080/api/jelovnik/${id}`);
  }

  resetFood(food: Food) {
    food.cena=0;
    food.naziv='';

  }

  updateFood(foodDto:FoodDto){
    console.log(foodDto);
      return this.http.put<any>(`http://localhost:8080/api/jelovnik/${foodDto.id}`,foodDto);
  }


 search(pretraga:string){
      return this.http.get<any>(`http://localhost:8080/api/jelovnik/?naziv=${pretraga}`);
  }

}

