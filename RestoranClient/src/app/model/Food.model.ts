import { Kategorija } from './Kategorija.model';

export class Food implements FoodInterface {
  public id?: number;
  public naziv: String;
  public kategorija: Kategorija;
  public cena:number;
 

  constructor(miCfg: FoodInterface) {
    this.id = miCfg.id;
    this.naziv = miCfg.naziv;
    this.kategorija = miCfg.kategorija;
    this.cena=miCfg.cena;
  
  }
}

export interface FoodInterface {
  id?: number;
  naziv: String;
  kategorija: Kategorija;
  cena: number;
}
