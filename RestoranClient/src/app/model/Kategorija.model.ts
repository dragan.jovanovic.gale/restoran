export class Kategorija implements KategorijaInterface {
    public id?: number;
    public naziv: string;
    
  
    constructor(miCfg: KategorijaInterface) {
      this.id = miCfg.id;
      this.naziv = miCfg.naziv;
    }
  }
  
  export interface KategorijaInterface {
    id?: number;
    naziv: string;
  }
  