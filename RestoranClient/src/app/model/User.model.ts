export class User implements UserInterface {
  public id?: number;
  public korisnickoIme: string;
  public eMail: string;
  public lozinka: string;
  public ime: string;
  public prezime: string;
  public ponovljenaLozinka: string;

  constructor(miCfg: UserInterface) {
    this.id = miCfg.id;
    this.korisnickoIme = miCfg.korisnickoIme;
    this.eMail = miCfg.eMail;
    this.lozinka = miCfg.lozinka;
    this.ponovljenaLozinka=miCfg.ponovljenaLozinka;
    this.ime=miCfg.ime;
    this.prezime=miCfg.prezime;
  }
}

export interface UserInterface {
  id?: number;
  korisnickoIme: string;
  eMail: string;
  lozinka: string;
  ime: string;
  prezime: string;
  ponovljenaLozinka: string;
}
