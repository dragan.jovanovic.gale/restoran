export class FoodDto  {
    public id:number|undefined;
    public naziv: String;
    public cena: number;
  
    constructor(id:number|undefined,naziv:String,cena:number) {
      this.id=id;
      this.naziv = naziv;
      this.cena = cena;
    }
  }
  