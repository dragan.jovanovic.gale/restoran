import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { User } from '../model/User.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public user: User;

  lozinka: string;

  ponovljenaLozinka: string;

  constructor(private appService: AppService, private route: Router) {
    this.user = {

      ime:'',
      prezime:'',
      korisnickoIme: '',
      eMail: '',
      lozinka: '',
      ponovljenaLozinka:'',
    };
    this.lozinka='';
    this.ponovljenaLozinka='';
  }

  ngOnInit(): void {
  }
 

  checkSame(password: string) {
    this.lozinka = this.lozinka;
    this.ponovljenaLozinka=this.ponovljenaLozinka;
    if (this.lozinka === this.ponovljenaLozinka) {
  }

  }

  register(){
  
    if(this.lozinka === this.ponovljenaLozinka){
      this.user.lozinka=this.lozinka;
      this.user.ponovljenaLozinka=this.ponovljenaLozinka;
      this.appService.register(this.user).subscribe(res=>console.log(res));
      this.route.navigate(['/login']);
    }else{
      alert('Lozinke nisu iste')
    }
    
}
}